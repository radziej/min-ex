import os

# setup database paths
# SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(instance, 'aachen3a.db')
# SQLALCHEMY_MIGRATE_REPO = os.path.join(instance, 'dbrepository')
# turn off tracking to avoid major costs
SQLALCHEMY_TRACK_MODIFICATIONS = False

# pagination
PER_PAGE = 10
