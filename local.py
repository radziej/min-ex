#!flask/bin/python
'''Executable script for local testing

This script can be executed to start the Flask server locally. It will then be
available at http://localhost:5000/. This should not be used for production, as
it is not deemed safe.
'''

# execute the flask application
from app import app
app.run()
