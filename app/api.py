# stdlib imports
import datetime
import flask

# local imports
from . import app, session, models, schemas


# Handle invalid API usage
class APIError(Exception):
    def __init__(self, message, status_code=400, payload=None):
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        # build dict from payload (or empty) and attach message
        rv = dict(self.payload or ())
        rv['message'] = self.message
        # return dict
        return rv


@app.errorhandler(APIError)
def handle_apierror(error):
    response = flask.jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


# Supportive functions
def get_model(name):
    '''Retrieves the model class

    This function retrieves the model from the models module based on the name
    of the model class. An APIError is raised, if it cannot be found.
    '''

    model = getattr(models, name, None)
    if not model:
        raise APIError('Invalid model: {0}'.format(name))
    return model


def get_schema(name):
    '''Retrieves the model schema instance

    This function retrieves the schema from the schemas module based on the
    name of the model class. An APIError is raised, if it cannot be found.
    '''

    schema = getattr(schemas, 'schema_' + name, None)
    if not schema:
        raise APIError('Invalid schema: {0}'.format(name))
    return schema


def query_unique(classname, id=None, name=None):
    '''Performs a query for a unique database entry

    If neither id nor name are provided, an error is raised.
    '''

    # Ensure that either the name or ID is given
    if not name and not id:
        raise APIError('Operation requires an ID or a name')
    # Ensure that id is used for Skims
    if name and 'Skim' in classname:
        raise APIError('Cannot query Skim by name')

    # Retrieve database entry
    model = get_model(classname)
    if id:
        entry = session.query(model).filter_by(id=id).one_or_none()
    else:
        entry = session.query(model).filter_by(name=name).one_or_none()
    if not entry:
        raise APIError('Could not find sample: {0}'.format(id or name))

    # Return result
    return entry


def validated_instance(classname):
    '''Create a class instance from classname and request JSON

    Taking the JSON payload from the request, a class instance of `classname `
    is created using the appropriate schema. This implies that all fields are
    validated. An error is raised, if validation fails.
    '''

    # Load JSON into schema (without any errors)
    schema = get_schema(classname)
    entry, errors = schema.load(flask.request.json)
    if errors:
        raise APIError('Fields are not configured correctly.', payload=errors)

    # Return entry class instance
    return entry


# Filter set class
class FilterSet(object):
    '''Set of filters which can be applied to a query

    This class applies filters in the form of 3-tuples to queries.
    '''

    def __init__(self, model, filters):
        # try to initialize model
        self.model = model
        self.filters = filters


    def parse_filter(self, entry):
        # ensure that all 3 expected fields are available
        if (not 'field' in entry or
            not 'value' in entry or
            not 'operator' in entry):
            raise APIError('Invalid filter {0}'.format(entry))
        # return 3 fields
        return entry['field'], entry['value'], entry['operator']


    def filter_query(self, query):
        '''Filter query by list of filter tuples

        Implementation heavily inspired by:
        http://stackoverflow.com/questions/14845196/dynamically-constructing-filters-in-sqlalchemy
        '''

        # Loop over all tuples of filters
        for entry in self.filters:
            # get components of filter
            field, value, operator = self.parse_filter(entry)
            # retrieve column
            column = getattr(self.model, field, None)
            if not column:
                raise APIError('Invalid column: {0}'.format(field))

            # filter(function, list_to_apply_function_to) for matching function
            try:
                function = filter(
                    # check for existing column functions
                    lambda f: hasattr(column, f.format(operator)),
                    # column function name templates
                    ['{0}', '{0}_', '__{0}__']
                )[0].format(operator)
            except IndexError:
                raise APIError('Invalid filter operator: {0}'.format(operator))

            # build filter operation and apply to query
            operation = getattr(column, function)(value)
            query = query.filter(operation)
        # return filtered query
        return query


# API Routes
@app.route('/api/v1.0/test', methods=['GET'])
def test():
    for entry in flask.request.json:
        print(entry)
    return flask.jsonify(flask.request.json)


@app.route('/api/v1.0/<string:classname>/<int:id>', methods=['GET'])
@app.route('/api/v1.0/<string:classname>/<string:name>', methods=['GET'])
def get_entry(classname, id=None, name=None):
    '''Direct access to database entries through their ID or name

    This function provides direct access to any entry in the database by their
    unique ID or their unique name (if the entry is a sample).
    '''

    # Get entry
    entry = query_unique(classname, id, name)

    # Serialize and return entry
    schema = get_schema(classname)
    return flask.jsonify(schema.dump(sample).data)


@app.route('/api/v1.0/query', methods=['GET'])
def get_query():
    '''Powerful query interface based on JSON

    This function provides a query interface mediated by the JSON payload of
    the GET request.
    '''

    # Filter query according to request JSON
    query = None
    return_schema = None
    for classname in sorted(flask.request.json):
        # Determine type of entry
        model = get_model(classname)
        # Initialize query and set schema to return with
        if not query:
            query = session.query(model)
            return_schema = get_schema(classname)
        else:
            query = query.join(model)

        # Create FilterSet and update query using it
        filterset = FilterSet(model, flask.request.json[classname])
        query = filterset.filter_query(query)

    # Execute query
    samples = query.all()
    return flask.jsonify(return_schema.dump(samples, many=True).data)


@app.route('/api/v1.0/<string:classname>', methods=['POST'])
def post_sample(classname):
    # Create entry class instance from payload
    entry = validated_instance(classname)

    # Add entry to database, commit and return
    session.add(entry)
    session.commit()
    return flask.jsonify({})


@app.route('/api/v1.0/<string:sample>/<int:id>/<string:skim>', methods=['POST'])
@app.route('/api/v1.0/<string:sample>/<string:name>/<string:skim>', methods=['POST'])
def post_skim(classname, id=None, name=None):
    # Query parent sample
    sample = query_unique(classname, id, name)

    # Load and vaildate skim
    skim = validated_instance(skim)

    # Insert Skim, commit and return
    sample.skims.append(skim)
    session.commit()
    return flask.jsonify({})


@app.route('/api/v1.0/<string:classname>/<int:id>', methods=['PUT'])
@app.route('/api/v1.0/<string:classname>/<string:name>', methods=['PUT'])
def put_entry(classname, id=None, name=None):
    # Query for entry
    entry = query_unique(classname, id, name)

    # Validate input and update entry
    schema = get_schema(classname)
    updated_entry, errors = schema.load(flask.request.json, instance=entry)
    if errors:
        session.rollback()
        raise APIError('Fields are not configured correctly.', payload=errors)

    session.commit()
    return flask.jsonify({})


@app.route('/api/v1.0/<string:classname>/<int:id>', methods=['DELETE'])
@app.route('/api/v1.0/<string:classname>/<string:name>', methods=['DELETE'])
def delete_entry(classname, id=None, name=None):
    # Query for entry
    entry = query_unique(classname, id, name)

    # Delete entry and commit changes to database
    session.delete(entry)
    session.commit()
    return flask.jsonify({})
