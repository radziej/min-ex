# flask and sqlalchemy imports
import flask
import sqlalchemy
import sqlalchemy.orm
import flask_sqlalchemy_session


# setup Flask and allow config for current instance
app = flask.Flask(__name__, instance_relative_config=True)
# default config from python object
app.config.from_object('cfg.default')
# load config from instance directory silently; if it doesnt exist (e.g.
# because this is not a self contained instance), ignore this call
# gracefully/silently
app.config.from_pyfile('config.py', silent=True)

# setup database engine from config
sqla_engine = sqlalchemy.create_engine(app.config['SQLALCHEMY_DATABASE_URI'])
# connect to database engine with scoped session
session_factory = sqlalchemy.orm.sessionmaker(bind=sqla_engine)
session = flask_sqlalchemy_session.flask_scoped_session(session_factory, app)

# load essentials
from . import views, models, api
