from marshmallow_sqlalchemy import ModelSchema
from marshmallow import fields
from . import session, models


# Schemas for model (de)serialization
class FileSchema(ModelSchema):
    id = fields.Int(dump_only=True)
    class Meta:
        model = models.File
        sqla_session = session


class DataSkimSchema(ModelSchema):
    id = fields.Int(dump_only=True)
    # Overwriting file relationship to embed nested entries
    files = fields.Nested(FileSchema, many=True)
    class Meta:
        model = models.DataSkim
        sqla_session = session


class MCSkimSchema(ModelSchema):
    id = fields.Int(dump_only=True)
    # Overwriting file relationship to embed nested entries
    files = fields.Nested(FileSchema, many=True)
    class Meta:
        model = models.MCSkim
        sqla_session = session


class DataSampleSchema(ModelSchema):
    id = fields.Int(dump_only=True)
    # Overwriting skim relationship to embed nested entries
    skims = fields.Nested(DataSkimSchema, many=True)
    class Meta:
        model = models.DataSample
        sqla_session = session


class MCSampleSchema(ModelSchema):
    id = fields.Int(dump_only=True)
    # Overwriting skim relationship to embed nested entries
    skims = fields.Nested(MCSkimSchema, many=True)
    class Meta:
        model = models.MCSample
        sqla_session = session


# Initialize schemas
schema_DataSample = DataSampleSchema()
schema_MCSample = MCSampleSchema()
schema_DataSkim = DataSkimSchema()
schema_MCSkim = MCSkimSchema()
schema_File = FileSchema()
