# stdlib imports
import os

# sqlalchemy imports
from sqlalchemy import Column, Integer, Float, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

# define Base object
Base = declarative_base()

class Sample(Base):
    '''Base class for sample objects

    This class should not be used on its own, but serve as a basis to inherit
    from.
    '''

    __tablename__ = 'sample'

    # unique identifier
    id = Column(Integer, primary_key=True)
    type = Column(String(20))
    # user
    updater = Column(String(20), index=True)
    comments = Column(String(300))
    # sample
    name = Column(String(150), unique=True)
    energy = Column(Integer)

    # declare mapper information
    __mapper_args__ = {
        'polymorphic_identity': 'sample',
        'polymorphic_on': type
    }


class DataSample(Sample):
    '''Sample class for data

    This class is based on Sample().
    '''

    __tablename__ = 'datasample'

    # id
    id = Column(Integer, ForeignKey('sample.id'), primary_key=True)
    # relation to DataSkim objects
    skims = relationship('DataSkim', backref='sample',
                         cascade='all, delete-orphan', lazy='dynamic')

    # declare mapper information
    __mapper_args__ = {
        'polymorphic_identity': 'datasample'
    }

    def __repr__(self):
        return '<DataSample {0}>'.format(self.id)


class MCSample(Sample):
    '''Sample class for data

    This class is based on Sample().
    '''

    __tablename__ = 'mcsample'

    # id
    id = Column(Integer, ForeignKey('sample.id'), primary_key=True)
    # relation to MCSkim objects
    skims = relationship('MCSkim', backref='sample',
                         cascade='all, delete-orphan', lazy='dynamic')
    # generator
    generator = Column(String(30), index=True)
    filterefficiency = Column(Float)
    filterefficiency_ref = Column(String(300))
    # cross section
    crosssection = Column(Float)
    crosssection_order = Column(String(10))
    crosssection_ref = Column(String(300))
    k_factor = Column(Float)
    k_factor_order = Column(String(10))
    k_factor_ref = Column(String(300))


    # declare mapper information
    __mapper_args__ = {
        'polymorphic_identity': 'mcsample'
    }

    def __repr__(self):
        return '<MCSample {0}>'.format(self.id)


class Skim(Base):
    '''Base class for skim objects

    This class should not be used on its own, but serve as a basis to inherit
    from.
    '''

    __tablename__ = 'skim'

    # unique identifier and parent sample
    id = Column(Integer, primary_key=True)
    type = Column(String(20))
    # user
    owner = Column(String(20), index=True)
    updater = Column(String(20), index=True)
    comments = Column(String(300))
    # sample
    datasetpath = Column(String(300), index=True)
    nevents = Column(Integer)
    sites = Column(String(100))
    files = relationship('File', cascade='all, delete-orphan', lazy='dynamic')
    # skimmer
    version = Column(String(40), index=True)
    cmssw = Column(String(40), index=True)
    globaltag = Column(String(80), index=True)
    # time and status
    created = Column(DateTime)
    finished = Column(DateTime)
    deprecated = Column(DateTime)
    private = Column(String(200), index=True)

    # declare mapper information
    __mapper_args__ = {
        'polymorphic_identity': 'skim',
        'polymorphic_on': type
    }


class DataSkim(Skim):
    '''Skim class for data

    This class is based on Skim().
    '''

    __tablename__ = 'dataskim'

    # ids
    id = Column(Integer, ForeignKey('skim.id'), primary_key=True)
    sample_id = Column(Integer, ForeignKey('datasample.id'))

    # run ranges
    run_json = Column(String(50))
    run_first = Column(Integer)
    run_last = Column(Integer)
    # luminosity
    luminosity = Column(Float)
    luminosity_ref = Column(String(0))
    luminosity_json = Column(String(700))

    __mapper_args__ = {
        'polymorphic_identity': 'dataskim'
    }

    def __repr__(self):
        return '<DataSkim {0}>'.format(self.id)


class MCSkim(Skim):
    '''Skim class for Monte Carlo

    This class is based on Skim().
    '''

    __tablename__ = 'mcskim'

    # inherit id
    id = Column(Integer, ForeignKey('skim.id'), primary_key=True)
    sample_id = Column(Integer, ForeignKey('mcsample.id'))

    __mapper_args__ = {
        'polymorphic_identity': 'mcskim'
    }

    def __repr__(self):
        return '<MCSkim {0}>'.format(self.id)


class File(Base):
    '''Class for skim files

    This class contains the paths and number of events for individual files
    stored on the dcache and linked to a specific skim.
    '''

    __tablename__ = 'file'

    # ids
    id = Column(Integer, primary_key=True)
    skim_id = Column(Integer, ForeignKey('skim.id'))
    # file
    path = Column(String(700))
    size = Column(Integer)
    nevents = Column(Integer)

    def __repr__(self):
        return '<File {0}, nevents={1}>'.format(self.id, self.nevents)
