# stdlib imports
from flask import request, send_from_directory, render_template

# local imports
from . import app, session, models


# Static files
# @app.route('/sitemap.xml')
@app.route('/robots.txt')
def send_file():
    return send_from_directory(app.static_folder, request.path[1:])


# Home
@app.route('/')
@app.route('/index')
def index():
    user = {'nickname': 'Benis'}
    posts = [  # fake array of posts
        {'author': {'nickname': 'John'},
         'body': 'Beautiful day in Portland!'},
        {'author': {'nickname': 'Susan'},
         'body': 'The Avengers movie was so cool!'},
        {'author': {'nickname': 'Benis'},
         'body': 'Benis benis benis!'}
    ]
    return render_template('index.html',
                           title='Home',
                           user=user,
                           posts=posts)


# MC Samples
@app.route('/mcsamples')
@app.route('/mcsamples/<int:page>')
def mcsamples(page=1):
    samples = session.query(models.MCSample)\
                     .order_by(models.MCSample.id.desc())\
                     .offset((page - 1) * app.config['PER_PAGE'])\
                     .limit(app.config['PER_PAGE'])
    return render_template('samples.html',
                           title='MC Samples',
                           samples=samples)
